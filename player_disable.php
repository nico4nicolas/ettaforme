<?php session_start(); ?>

<?php require_once "common/classPlayers.php"; ?>

<?php ob_start(); ?>

<?php

if (!($_SESSION['is_logged'] == true)) {
	echo "<div class=\"alert warning\">Vous n'êtes pas autorisé à voir cette page !</div>";
}
else {
	$do_action = false;
	$error_message = '<div class="alert warning">Erreur : données joueur manquantes !</div>';

	if(isset ( $_GET['action'] ) && !empty( $_GET['action'] ) ) {
		if( $_GET['action'] == 'disable') {
			if(isset( $_GET['tid'] ) && !empty( $_GET['tid'] ) ) {
				$team_id = $_GET['tid'];
			
				if( isset( $_GET['pid'] ) && !empty( $_GET['pid'] )) {
					$players_obj = new Players($team_id);
					$player_obj = $players_obj->get_player_object($_GET['pid']);
					if ( false != $player_obj ) {
						$player_obj->reset_code();
						$do_action = true;
					}
				}
			}
		}
		else {
			$error_message = '<div class="alert warning">Erreur : action invalide!</div>';
		}
	}
	else {
		$error_message = '<div class="alert warning">Erreur : action invalide!</div>';
	}

	if ( $do_action == true ) {
		if ($players_obj->update()) {
			echo '<div class="alert success">'.$player_obj->get_fullname().' désactivé !</div>';
		}
		else {
			echo '<div class="alert error">Erreur : joueur non désactivé !</div>';
		}
	}
	else {
		echo $error_message;
	}
	echo '<a class="button" href="display_players.php?tid='.$team_id.'">Retour</a>';
}
?>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>