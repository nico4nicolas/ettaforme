<?php
# --- General ---
$g_default_language		= 'fr';
$g_version				= '1.0.1';

# --- Branding ---
$g_website_title		= 'Et ta forme ?';

# --- Branding (in navbar) ---
$g_website_name			= $g_website_title;
$g_homepage				= 'index.php';
$g_logo_image			= 'img/logo.png';
$g_logo_link			= '#';

# --- Options ---
$g_nb_questions			= 8;

# --- Lists ---
$g_list_example = array(
	'1',
	'2',
	'3',
	'4',
	'5',
	'6',
);
?>
