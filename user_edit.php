<?php session_start(); ?>

<?php require_once "common/classUsers.php"; ?>

<?php ob_start(); ?>

<?php

if (!($_SESSION['is_logged'] == true)) {
	echo "<div class=\"alert warning\">Vous n'êtes pas autorisé à voir cette page !</div>";
}
else {
	$do_action = false;
	$error_message = '<div class="alert warning">Erreur : données manquantes !</div>';

    do {
        /* action parameter should be set and not empty */
        if( !isset ( $_POST['action'] ) || empty( $_POST['action'] ) ) {
            $error_message = '<div class="alert error">Erreur : action manquante !</div>';
            break;
        }

        /* only update is allowed / handled */
        if( $_POST['action'] != 'update' ) {
            $error_message = '<div class="alert error">Erreur : action non gérée !</div>';
            break;
        }

        /* check if current user exist */
        $users_obj = new Users();
        $user_obj = $users_obj->get_user_object($_SESSION['username']);

        if ( false == $user_obj ) {
            $error_message = '<div class="alert error">Erreur : l\'utilisateur actuel n\'a pas été trouvé !</div>';
            break;
        }

        $is_curr_user_admin = $user_obj->get_is_admin();

        /* only allow to modify oneself unless user is admin */
        if ( $_POST['uid'] != $_SESSION['username'] ) {
            if ( true != $is_curr_user_admin ) {
                $error_message = '<div class="alert error">Erreur : pas de droit admin !</div>';
                break;
            }
        }

        /* get user to update object */
        $user_obj = $users_obj->get_user_object($_POST['uid']);

        if ( false == $user_obj ) {
            $error_message = '<div class="alert error">Erreur : utilisateur '.$_POST['uid'].' non trouvé !</div>';
            break;
        }
        /* change password */
        if ( isset($_POST['upwd_new']) && !empty( $_POST['upwd_new'] ) ) {
            if ( true == $is_curr_user_admin || ($user_obj->is_password_correct($_POST['upwd'])) ) {
                $user_obj->set_password($_POST['upwd_new']);
                $do_action = true;
            }
            else {
                $error_message = '<div class="alert warning">Erreur : mot de passe incorrect !</div>';
            }
        }
        /* change other data */
        $user_obj->set_firstname($_POST['ufname']);
        $user_obj->set_lastname($_POST['ulname']);
        $do_action = true;
    } while (0);

	if ( $do_action == true ) {
		if ($users_obj->update()) {
			echo '<div class="alert success">Les informations ont été modifiées !</div>';
		}
		else {
			echo '<div class="alert error">Erreur : informations non modifiées !</div>';
		}
	}
	else {
		echo $error_message;
	}

    if ( true == $is_curr_user_admin ) {
        echo '<a class="button" href="display_users.php">Retour</a>';
    }
    else {
	    echo '<a class="button" href="display_profile.php">Retour</a>';
    }
}
?>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>