<?php session_start(); ?>

<?php require_once "config/config.php"; ?>
<?php require_once "common/classPlayers.php"; ?>

<?php ob_start(); ?>

<?php

if(isset( $_POST['team'] ) && !empty( $_POST['team'] ) ) {
	$team_id = $_POST['team'];
	if(isset( $_POST['player'] ) && !empty( $_POST['player'] ) ) {
		$players_obj = new Players($team_id);
		$player_obj = $players_obj->get_player_object($_POST['player']);
	}
	else {
		echo "<div class=\"alert warning\">Pas de joueur sélectionné !</div>";
	}
}
else {
	echo "<div class=\"alert warning\">Pas d'équipe sélectionnée !</div>";
}

if ( $player_obj->is_code_correct($_POST["code"]) ) {
?>
	<form id="regForm" action="page_answers.php" method="POST">

		<input type="hidden" id="team" name="team" value="<?=$team_id?>" />
		<input type="hidden" id="player" name="player" value="<?=$player_obj->get_id()?>" />
		<input type="hidden" id="code" name="code" value="<?=$_POST["code"]?>" />

		<div class="all-steps" id="all-steps">
<?php
		for ( $i = 1; $i <= $g_nb_questions; $i++ ) {
			echo "<span class=\"step\"></span>";
		}
		echo '</div>';

		for ( $i = 1; $i <= $g_nb_questions; $i++ ) {
			echo "<div class=\"tab\">";
			include "data/questions/q".$i.".html";
			echo "</div>";
		}
?>
		<div style="overflow:auto;">
			<button type="button" id="prevBtn" onclick="nextPrev(-1)">Précédent</button>
			<button type="button" id="nextBtn" onclick="nextPrev(1)">Suivant</button>
		</div>
	</form>
<?php
}
else {
	echo '<div class="alert error">Code incorrect !</div>';
	echo '<a class="button" id="prevBtn" href="page_team.php">Retour</a>';
}

?>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>