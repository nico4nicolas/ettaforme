<?php session_start(); ?>

<?php require_once "common/classUsers.php"; ?>

<?php ob_start(); ?>

<?php

$display_page = false;
$error_message = '<div class="alert warning">Vous n\'êtes pas autorisé à voir cette page !</div>';

if (($_SESSION['is_logged'] == true)) {

	$users_obj = new Users();
	$user_obj = $users_obj->get_user_object($_SESSION['username']);
	if ($user_obj != false) {
		if ($user_obj->get_is_admin() == true) {
			$display_page = true;
		}
	}
}

if ($display_page == true) {

	echo '<table>';
	
	echo '<tr><td colspan="100%">';
	
	$modal_id = 'modal_adduser';
	
	echo '<label for="'.$modal_id.'" class="button success">➕ Ajouter un utilisateur</label>';
	
	echo '<div class="modal">';
	echo '  <input id="'.$modal_id.'" type="checkbox" />';
	echo '  <label for="'.$modal_id.'" class="overlay"></label>';
	echo '  <article>';
	echo '	<header>';
	echo '	  <h3>Ajouter un utilisateur</h3>';
	echo '	  <label for="'.$modal_id.'" class="close">&times;</label>';
	echo '	</header>';
	echo '	<section class="content">';
	echo '		<form action="user_add.php" method="post" name="user_add">';
	echo '			<input type="hidden" id="action" name="action" value="insert" />';
	echo '			<label for="uid">Id</label></br>';
	echo '			<input type="text" id="uid" name="uid" required /></br>';
	echo '			<label for="ufname">Prénom</label></br>';
	echo '			<input type="text" id="ufname" name="ufname" /></br>';
	echo '			<label for="ulname">Nom</label></br>';
	echo '			<input type="text" id="ulname" name="ulname" /></br>';	
	echo '			<label for="upwd">Mot de passe</label></br>';
	echo '			<input type="password" id="upwd" name="upwd" /></br>';
	echo '			<button type="submit">Ajouter</button>';
	echo '			<label for="'.$modal_id.'" class="button dangerous">Annuler</label>';
	echo '		</form> ';
	echo '	</section>';
	echo '  </article>';
	echo '</div>';
	
	echo '</td></tr>';

	echo '<tr><th>Id</th><th>Utilisateur</th><th>Admin</th><th>Dernière connexion</th><th>Nombre connexions</th><th>Actions</th></tr>';
	
	foreach ($users_obj->get_list_users() as $u) {

		echo '<tr>';
		echo '<td>'.$u->get_id().'</td>';
		echo '<td>'.$u->get_fullname().'</td>';
		echo '<td>';
		if ($u->get_is_admin()) {
			echo '✅';
		}
		echo '</td>';
		echo '<td>'.$u->get_last_login().'</td>';
		echo '<td>'.$u->get_number_logins().'</td>';
		echo '<td>';
		
		$modal_id = 'modal_edit_'.$u->get_id();
		
		echo '<label for="'.$modal_id.'" class="button warning">Modifier</label> ';

		/* TODO : ajouter la fonction de désaction d'un utilisateur
		if ($u->is_password_correct("")) {
			echo '<a class="button" disabled>Désactiver</a>';
		}
		else {
			//echo '<a class="button error" onclick="return confirm(\'Désactiver '.$u->get_firstname().' ?\');" href="user_disable.php?action=disable&uid='.$u->get_id().'">🗑</a>';
			echo '<a class="button error">Désactiver</a>';
		}
		*/
		
		echo '<div class="modal">';
		echo '  <input id="'.$modal_id.'" type="checkbox" />';
		echo '  <label for="'.$modal_id.'" class="overlay"></label>';
		echo '  <article>';
		echo '	<header>';
		echo '	  <h3>Modifier un utilisateur</h3>';
		echo '	  <label for="'.$modal_id.'" class="close">&times;</label>';
		echo '	</header>';
		echo '	<section class="content">';
		echo '		<form action="user_edit.php" method="post" name="user_edit">';
		echo '			<input type="hidden" id="action" name="action" value="update" />';
		echo '			<label for="uid">Id</label></br>';
		echo '			<input type="text" id="uid" name="uid" required readonly value="'.$u->get_id().'" /></br>';
		echo '			<label for="ufname">Prénom</label></br>';
		echo '			<input type="text" id="ufname" name="ufname" value="'.$u->get_firstname().'" /></br>';
		echo '			<label for="ulname">Nom</label></br>';
		echo '			<input type="text" id="ulname" name="ulname" value="'.$u->get_lastname().'" /></br>';
		echo '			<label for="upwd_new">Mot de passe</label></br>';
		echo '			<input type="password" id="upwd_new" name="upwd_new" /></br>';
		echo '			<button type="submit">Modifier</button>';
		echo '	  		<label for="'.$modal_id.'" class="button dangerous">Annuler</label>';
		echo '		</form> ';
		echo '	</section>';
		echo '  </article>';
		echo '</div>';
		
		echo '</td></tr>';
	}
	echo '</table>';
}

?>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>