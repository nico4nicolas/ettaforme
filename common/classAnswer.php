<?php

class Answer {
	/* Public properties */
	public $date;
	public $time;
	public $player_id;
	public $values;

	/* Private properties */

	/* Construct */
	function __construct($date, $time, $player_id, $values) {
		$this->date = $date;
		$this->time = $time;
		$this->player_id = $player_id;
		$this->values = $values;
	}
	
	/* Methods */
}

?>