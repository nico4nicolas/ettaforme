<?php

require_once 'classTeam.php';

class Teams {
	/* Public properties */
	public $nb_teams;
	public $array_teams;
	
	/* Private properties */
	private $filepath;

	/* Construct */
	function __construct() {
		$this->nb_teams = 0;
		$this->array_teams = array();
		$this->filepath = "data/teams/teams.json";
		$this->get_list_of_teams();
	}
	
	/* Methods */
	private function get_list_of_teams_csv() {
		$file = fopen("data/teams.csv","r");
		$nb_lines = 0;
		
		while (!feof($file)) {
			$line = fgetcsv($file);
			if ($line !== false) {
				if (2 == count($line)) {
					if ($nb_lines > 0) {
						$team = new Team($line[0], $line[1]);
						array_push($this->array_teams, $team);
						$this->nb_teams += 1;
					}
				}
				$nb_lines += 1;
			}
		}
		fclose($file);
	}

	private function get_list_of_teams() {
        if(file_exists($this->filepath)) {
            $json_data = file_get_contents($this->filepath);
            $data = json_decode($json_data, true);

			foreach ($data as $json_team) {
				$team = new Team($json_team["id"], $json_team["name"]);
				array_push($this->array_teams, $team);
				$this->nb_teams += 1;
			}
		}
    }

	private function is_team_present($id) {
	    foreach ($this->array_teams as $t) {
	        if ($t->get_id() == $id) {
	            return true;
	        }
	    }
	    return false;
	}

	public function insert($new_team) {
        if((!empty($new_team)) && ($new_team instanceof Team)) {
			if (!$this->is_team_present($new_team->id)) {
				if(!empty($this->array_teams)) {
					array_push($this->array_teams, $new_team);
				}
				else {
					$this->array_teams[] = $new_team;
				}
				$insert = file_put_contents($this->filepath, json_encode($this->array_teams, JSON_PRETTY_PRINT));

				if ($insert == true) {
					$this->nb_teams += 1;
				}

				return $insert?true:false;
			}
			else {
				return false; /* team id already exists */
			}
        }
		else {
            return false; /* new team is empty */
        }
    }

	public function update($id, $team) {
        if(!empty($team) && !empty($id)) {            
            foreach ($this->array_teams as $t) {
                if ($t->get_id() == $id) {
                    if(isset($team->name)) {
                        $t->name = $team->get_name();
                    }
                }
            }
            $update = file_put_contents($this->filepath, json_encode($this->array_teams, JSON_PRETTY_PRINT)); 
			
            return $update?true:false;
        }
		else {
            return false;
        }
    }

    public function delete($id) {
		$this->array_teams = array_filter($this->array_teams, function($a) use($id) {
			return $a->id !== $id;
		});
        $delete = file_put_contents($this->filepath, json_encode($this->array_teams, JSON_PRETTY_PRINT));

		if ($delete == true) {
			$this->nb_teams -= 1;
		}

        return $delete?true:false;
    }
	
	public function get_team_name($team_id) {
	    foreach ($this->array_teams as $t) {
	        if ($t->get_id() == $team_id) {
	            return $t->get_name();
	        }
	    }
	    return "";
	}
}

?>
