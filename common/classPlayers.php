<?php

require_once 'classPlayer.php';

class Players {
	/* Public properties */
	public $team_id;
	public $nb_players;
	public $array_players;
	
	/* Private properties */
	private $filepath;

	/* Construct */
	function __construct($id) {
		$this->team_id = $id;
		$this->nb_players = 0;
		$this->array_players = array();
		$this->filepath = "data/teams/".$this->team_id."/players.json";
		$this->get_list();
	}
	
	/* Methods */
	private function get_list() {
        if(file_exists($this->filepath)) {
            $data_raw = file_get_contents($this->filepath);
            $data_json = json_decode($data_raw, true);

			foreach ($data_json as $player_json) {
				$player_obj = new Player($player_json["id"], $player_json["first_name"], $player_json["last_name"]);
				$player_obj->set_code($player_json["code"], true);
				array_push($this->array_players, $player_obj);
				$this->nb_players += 1;
			}
		}
    }

	private function is_player_present($id) {
	    foreach ($this->array_players as $p) {
	        if ($p->get_id() == $id) {
	            return true;
	        }
	    }
	    return false;
	}

	public function get_player_object($id) {
	    foreach ($this->array_players as $p) {
	        if ($p->get_id() == $id) {
	            return $p;
	        }
	    }
	    return false;
	}

	private function get_unique_id() {
		$unique_id = "";

		/* get unique identifier for reference */
		do {
			$unique_id = substr(uniqid(),-8);
		} while($this->is_player_present($unique_id));
		
		return $unique_id;
	}

	public function insert($first_name, $last_name) {
		$id = $this->get_unique_id();
		$new_player = new Player($id, $first_name, $last_name);

        if((!empty($new_player)) && ($new_player instanceof Player)) {
			if (!$this->is_player_present($new_player->get_id())) {
				if(!empty($this->array_players)) {
					array_push($this->array_players, $new_player);
				}
				else {
					$this->array_players[] = $new_player;
				}
				$insert = file_put_contents($this->filepath, json_encode($this->array_players, JSON_PRETTY_PRINT));

				return $insert?$id:false;
			}
			else {
				return false; /* player id already exists */
			}
        }
		else {
            return false; /* new player is empty */
        }
    }

	public function update() {
		$update = file_put_contents($this->filepath, json_encode($this->array_players, JSON_PRETTY_PRINT)); 
		return $update?true:false;
    }

    public function delete($id) {
		$this->array_players = array_filter($this->array_players, function($a) use($id) {
			return $a->get_id() !== $id;
		});
        $delete = file_put_contents($this->filepath, json_encode($this->array_players, JSON_PRETTY_PRINT));
        return $delete?true:false;
    }
	
	public function get_player_name($id) {
	    foreach ($this->array_players as $p) {
	        if ($p->get_id() == $id) {
	            return $p->get_fullname;
	        }
	    }
	    return "";
	}
}

?>
