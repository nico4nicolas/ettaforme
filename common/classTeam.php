<?php

class Team implements JsonSerializable {
	/* Public properties */

	/* Private properties */
	private $id;
	private $name;
	private $is_active;

	/* Construct */
	function __construct($id, $name) {
		$this->id = $id;
		$this->name = $name;
		$this->is_active = true;
	}
	
	/* Methods */
	public function jsonSerialize() {
		$array_data = [	'id' => $this->id,
						'name' => $this->name,
						'is_active' => $this->is_active
				];
		return $array_data;
	}

	public function get_id () {
		return $this->id;
	}

	public function get_name () {
		return $this->name;
	}

	public function set_name ($new_name) {
		$is_set = false;
		if (isset($new_name)) {
			$this->name = $new_name;
			$is_set = true;
		}
		return $is_set;
	}

	public function get_is_active () {
		return $this->is_active;
	}

	public function set_is_active ($new_status) {
		$is_set = false;
		if (is_bool($new_status)) {
			$this->is_active = $new_status;
			$is_set = true;
		}
		return $is_set;
	}
}

?>
