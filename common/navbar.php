<?php
echo '<nav>';
echo '<a href="'.$g_homepage.'" class="brand"><img class="logo" src="'.$g_logo_image.'" /><span>'.$g_website_name.'</span></a>';

echo '<input id="bmenub" type="checkbox" class="show">';
echo '<label for="bmenub" class="burger pseudo button">☰</label>';

echo '<div class="menu">';
echo '<a href="display_help.php" class="pseudo button">🏀 Aide</a>';

if (isset( $_SESSION['username']) && $_SESSION['is_logged'] == true) {
	echo '<a class="pseudo button" href="display_profile.php">👤 '.$_SESSION['username'].'</a>';
	echo '<a class="button" href="display_teams.php">Equipes</a>';
	echo '<a class="button" href="display_players.php">Effectifs</a>';
	echo '<a class="button" href="display_answers.php">Réponses</a>';
    echo '<a class="pseudo button" href="page_login.php?action=logout">🔒 Se déconnecter</a>';
}
else {
	echo '<label for="modal_login" class="pseudo button">🔒 Espace entraineur</label>';
}
  
echo '</div>';
echo '</nav>';
?>