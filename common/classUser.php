<?php

class User implements JsonSerializable {
	/* Public properties */

	/* Private properties */
	private $id;
	private $first_name;
	private $last_name;
	private $password;
	private $is_admin;
	private $last_login;
	private $nb_logins;

	/* Construct */
	function __construct($id, $first_name = "", $last_name = "") {
		$this->id = $id;
		$this->first_name = $first_name;
		$this->last_name = strtoupper($last_name);
		$this->password = sha1("");
		/* Non required data should be set after creating the object */
		$this->is_admin = false;
		$this->last_login = "";
		$this->nb_logins = 0;
	}
	
	/* Methods */
	public function jsonSerialize() {
		$array_data = [	'id' => $this->id,
						'first_name' => $this->first_name,
						'last_name' => $this->last_name,
						'password' => $this->password,
						'is_admin' => $this->is_admin,
						'last_login' => $this->last_login,
						'nb_logins' => $this->nb_logins
				];
		return $array_data;
	}

	public function get_id() {
		return $this->id;
	}

	public function get_firstname() {
		return $this->first_name;
	}

	public function set_firstname($fname) {
		$is_set = false;
		if(isset($fname)) {
			$this->first_name = $fname;
			$is_set = true;
		}
		return $is_set;
	}

	public function get_lastname() {
		return $this->last_name;
	}

	public function set_lastname($lname) {
		$is_set = false;
		if(isset($lname)) {
			$this->last_name = strtoupper($lname);
			$is_set = true;
		}
		return $is_set;
	}

	public function get_fullname() {
	    return $this->first_name.' '.$this->last_name;
	}

	public function set_password($password, $is_crypted = false) {
		$is_set = false;
		if(isset($password)) {
			if ($is_crypted == false) {
				$this->password = sha1($password);
			}
			else {
				$this->password = $password;
			}
			$is_set = true;
		}
		return $is_set;
	}

	public function get_is_admin() {
		return $this->is_admin;
	}

	public function set_is_admin($is_admin) {
		$is_set = false;
		if (is_bool($is_admin)) {
			$this->is_admin = $is_admin;
			$is_set = true;
		}
		return $is_set;
	}

	public function is_password_correct($password) {
		return sha1($password) == $this->password;
	}

	public function set_login($last, $nb) {
		$this->last_login = $last;
		$this->nb_logins = $nb;
	}

	public function set_new_login() {
		$date = new \DateTime();
		$this->last_login = $date->format('Y-m-d H:i:s');
		$this->nb_logins += 1;
	}

	public function get_last_login() {
		return $this->last_login;
	}

	public function get_number_logins() {
		return $this->nb_logins;
	}
}

?>
