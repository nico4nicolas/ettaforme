<?php

require_once 'classAnswer.php';

class Answers {
	/* Public properties */
	public $team_id;
	public $nb_answers;
	public $array_answers;
	
	/* Private properties */

	/* Construct */
	function __construct($id) {
		$this->team_id = $id;
		$this->nb_answers = 0;
		$this->array_answers = array();
		$this->get_answers_from_file($this->team_id);
	}
	
	/* Methods */
	private function get_answers_from_file($id) {
		$filepath = "data/teams/".$id."/answers.csv";
		
		if ( file_exists($filepath) ) {
			$file = fopen($filepath,"r");
			$nb_lines = 0;
			/* read the file until the end */
			while (!feof($file)) {
				$line = fgetcsv($file);
				if ($line !== false) {
					$nb_csv_fields = count($line);
					if ($nb_csv_fields >= 3) {
						if ($nb_lines > 0) {
							$values = array();
							if ($nb_csv_fields > 3) {
								for($i = 3; $i < $nb_csv_fields; $i++) {
									array_push($values, $line[$i]);
								}
							}
							$answer = new Answer($line[0],$line[1],$line[2],$values);
							array_push($this->array_answers, $answer);
							$this->nb_answers += 1;
						}
					}
					$nb_lines += 1;
				}
			}
			fclose($file);
		}
	}
}

?>