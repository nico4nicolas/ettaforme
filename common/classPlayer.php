<?php

class Player implements JsonSerializable {
	/* Public properties */

	/* Private properties */
	private $id;
	private $first_name;
	private $last_name;
	private $code;

	/* Construct */
	function __construct($id, $first_name, $last_name) {
		$this->id = $id;
		$this->first_name = $first_name;
		$this->last_name = strtoupper($last_name);
		$this->code = sha1("");
	}
	
	/* Methods */
	public function jsonSerialize() {
		$array_data = [	'id' => $this->id,
						'first_name' => $this->first_name,
						'last_name' => $this->last_name,
						'code' => $this->code
				];
		return $array_data;
	}

	public function get_id() {
		return $this->id;
	}

	public function get_firstname() {
		return $this->first_name;
	}

	public function set_firstname($fname) {
		$is_set = false;
		if(isset($fname)) {
			$this->first_name = $fname;
			$is_set = true;
		}
		return $is_set;
	}

	public function get_lastname() {
		return $this->last_name;
	}

	public function set_lastname($lname) {
		$is_set = false;
		if(isset($lname)) {
			$this->last_name = strtoupper($lname);
			$is_set = true;
		}
		return $is_set;
	}

	public function get_fullname() {
	    return $this->first_name.' '.$this->last_name;
	}

	public function set_code($code, $is_crypted = false) {
		$is_set = false;
		if( isset($code) && !empty($code) ) {
			if ($is_crypted == false) {
				$this->code = sha1($code);
			}
			else {
				$this->code = $code;
			}
			$is_set = true;
		}
		return $is_set;
	}

	public function reset_code() {
		$this->code = sha1("");
	}

	public function is_code_correct($code) {
		return sha1($code) == $this->code;
	}
}

?>
