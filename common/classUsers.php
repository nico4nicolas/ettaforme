<?php

require_once 'classUser.php';

class Users {
	/* Public properties */
	public $nb_users;
	
	/* Private properties */
	private $array_users;
	private $filepath;

	/* Construct */
	function __construct() {
		$this->nb_users = 0;
		$this->array_users = array();
		$this->filepath = "data/users.json";
		$this->get_list_from_file();
	}
	
	/* Methods */
	private function get_list_from_file() {
        if(file_exists($this->filepath)) {
            $json_data = file_get_contents($this->filepath);
            $data = json_decode($json_data, true);

			foreach ($data as $user_json) {
				$user_obj = new User(	$user_json["id"],
										$user_json["first_name"],
										$user_json["last_name"]);

				$user_obj->set_password($user_json["password"], true);
				
				$user_obj->set_login($user_json["last_login"],$user_json["nb_logins"]);

				if($user_json["is_admin"]  == true) {
					$user_obj->set_is_admin(true);
				}
				
				array_push($this->array_users, $user_obj);
				$this->nb_users += 1;
			}
		}
    }

	private function is_user_present($id) {
	    foreach ($this->array_users as $usr) {
	        if ($usr->get_id() == $id) {
	            return true;
	        }
	    }
	    return false;
	}

	public function get_list_users() {
		return $this->array_users;
	}

	public function get_user_object($id) {
	    foreach ($this->array_users as $usr) {
	        if ($usr->get_id() == $id) {
	            return $usr;
	        }
	    }
	    return false;
	}

	private function get_unique_id() {
		$unique_id = "";

		/* get unique identifier for reference */
		do {
			$unique_id = substr(uniqid(),-8);
		} while($this->is_user_present($unique_id));
		
		return $unique_id;
	}

	public function insert($id, $first_name, $last_name) {
		$new_user = new user($id, $first_name, $last_name);
        if((!empty($new_user)) && ($new_user instanceof User)) {
			if (!$this->is_user_present($new_user->get_id())) {
				if(!empty($this->array_users)) {
					array_push($this->array_users, $new_user);
				}
				else {
					$this->array_users[] = $new_user;
				}
				$insert = file_put_contents($this->filepath, json_encode($this->array_users, JSON_PRETTY_PRINT));
				return $insert?$id:false;
			}
			else {
				return false; /* user id already exists */
			}
        }
		else {
            return false;
        }
    }

	public function update() {
		$update = file_put_contents($this->filepath, json_encode($this->array_users, JSON_PRETTY_PRINT)); 
		return $update?true:false;
    }

	public function reset_code($id) {
	    foreach ($this->array_users as $usr) {
	        if ($usr->get_id() == $id) {
	            $usr->set_password("1234");
			}
	    }
		$update = file_put_contents($this->filepath, json_encode($this->array_users, JSON_PRETTY_PRINT)); 

		return $update?true:false;
	}

    public function delete($id) {
		$this->array_users = array_filter($this->array_users, function($a) use($id) {
			return $a->get_id() !== $id;
		});
        $delete = file_put_contents($this->filepath, json_encode($this->array_users, JSON_PRETTY_PRINT));
        return $delete?true:false;
    }
	
	public function get_user_fullname($user_id) {
		$fullname = "";
	    foreach ($this->array_users as $usr) {
	        if ($usr->get_id() == $user_id) {
	            $fullename = $usr->first_name.' '.$usr->last_name;
	        }
	    }
	    return $fullname;
	}
}

?>
