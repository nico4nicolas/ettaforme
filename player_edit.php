<?php session_start(); ?>

<?php require_once "common/classPlayers.php"; ?>

<?php ob_start(); ?>

<?php

if (!($_SESSION['is_logged'] == true)) {
	echo "<div class=\"alert warning\">Vous n'êtes pas autorisé à voir cette page !</div>";
}
else {
	$do_action = false;
	$error_message = '<div class="alert warning">Erreur : données manquantes !</div>';

	if(isset ( $_POST['action'] ) && !empty( $_POST['action'] ) ) {
		if( $_POST['action'] == 'update') {
			if( isset( $_POST['tid'] ) && !empty( $_POST['tid'] ) && isset( $_POST['pid'] ) && !empty( $_POST['pid'] ) ) {
				$team_id = $_POST['tid'];
				$players_obj = new Players($team_id);
				$player_obj = $players_obj->get_player_object($_POST['pid']);

				if ( false != $player_obj ) {
					$player_obj->set_firstname($_POST['pfname']);
					$player_obj->set_lastname($_POST['plname']);

					if ( isset( $_POST['pcode'] ) && !empty( $_POST['pcode'] ) ) {
						$player_obj->set_code($_POST['pcode']);
					}
					$do_action = true;
				}
			}
		}
	}
	else {
		if(isset( $_GET['tid'] ) && !empty( $_GET['tid'] ) ) {
			$team_id = $_GET['tid'];
			$players = new Players($team_id);
			if(isset( $_GET['pid'] ) && !empty( $_GET['pid'] ) ) {
				$player = $players->get_player_object($_GET['pid']);
			}
			else {
				$error_message = '<div class="alert warning">Erreur : données joueur manquantes !</div>';
			}
		}
		else {
			$error_message = '<div class="alert warning">Erreur : données équipe manquante !</div>';
		}
	}

	if ( $do_action == true ) {
		if ($players_obj->update()) {
			echo '<div class="alert success">'.$player_obj->get_fullname().' modifié !</div>';
		}
		else {
			echo '<div class="alert error">Erreur : joueur non modifié !</div>';
		}
	}
	else {
		echo $error_message;
	}
	echo '<a class="button" href="display_players.php?tid='.$team_id.'">Retour</a>';
}
?>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>