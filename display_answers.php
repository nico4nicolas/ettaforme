<?php session_start(); ?>

<?php include_once "config/config.php"; ?>
<?php require_once "common/classTeams.php"; ?>
<?php require_once "common/classPlayers.php"; ?>
<?php require_once "common/classAnswers.php"; ?>

<?php ob_start(); ?>
<?php

if (!($_SESSION['is_logged'] == true)) {
	echo "<div class=\"alert warning\">Vous n'êtes pas autorisé à voir cette page !</div>";
}
else {
	$display_page = false;
	$filter_pid = false;

	if(isset( $_GET['tid'] ) && !empty( $_GET['tid'] ) ) {
		$team_id = $_GET['tid'];
		$display_page = true;
		if(isset( $_GET['pid'] ) && !empty( $_GET['pid'] ) ) {
			$filter_pid = $_GET['pid'];
		}
	}
	else {
		echo "<div class=\"alert warning\">Pas d'équipe sélectionnée !</div>";
	}
	?>
		<form action="display_answers.php" method="GET">
			<label for="tid">Je veux voir les réponses des</label>
			<select name="tid" id="tid" onchange="this.form.submit()">
	<?php
			$teams = new Teams();
			foreach ($teams->array_teams as $t) {
			    if($t->get_id() == $team_id) {
			        echo "<option value=\"".$t->get_id()."\" selected>".$t->get_name()."</option>";
			        $selected_team_id = $t->get_id();
			        $selected_team_name = $t->get_name();
			    }
			    else {
				    echo "<option value=\"".$t->get_id()."\">".$t->get_name()."</option>";
			    }
			}
			if ( $display_page == false ) {
			    echo '<option value="" selected hidden></option>';
			}
	
			echo '</select>';
			
			if ( $display_page == true ) {
			    echo '<a class="button" onclick="tableToCSV()">Télécharger</a>';
			}
	?>
		</form>

	<?php
	if ( $display_page == true ) {
		$answers = new Answers($team_id);
		$players = new Players($team_id);

		echo "<table>";
		echo "<tr><th>Date</th><th>Heure</th><th>Joueur</th>";
		for ( $i = 1; $i <= $g_nb_questions; $i++ ) {
			echo "<th>Q".$i."</th>";
		}
		echo "</tr>";

		foreach ($answers->array_answers as $a) {
			$display_row = true;
			if (($filter_pid != false) && ($filter_pid != $a->player_id)) {
				$display_row = false;
			}
			if ($display_row == true) {
				$p = $players->get_player_object($a->player_id);
				echo "<tr>";
				echo "<td>".$a->date."</td>";
				echo "<td>".$a->time."</td>";
				echo '<td><a href="display_answers.php?tid='.$team_id.'&pid='.$a->player_id.'">'.$p->get_fullname().'</a></td>';

				foreach ( $a->values as $v ) {
					echo "<td class=\"level".$v."\">".$v."</td>";
				}
				echo "</tr>";
			}
		}
		echo "</table>";
	}
}
?>

<?php $content = ob_get_clean(); ?>

<script type="text/javascript">
function tableToCSV() {

	// Variable to store the final csv data
	var csv_data = [];

	// Get each row data
	var rows = document.getElementsByTagName('tr');
	for (var i = 0; i < rows.length; i++) {

		// Get each column data
		var cols = rows[i].querySelectorAll('td,th');

		// Stores each csv row data
		var csvrow = [];
		for (var j = 0; j < cols.length; j++) {
			if (cols[j].children.length == 0) {
				// Get the text data of each cell
				// of a row and push it to csvrow
				csvrow.push(cols[j].innerHTML);
			}
			else {
				csvrow.push(cols[j].firstElementChild.innerHTML);
			}
		}

		// Combine each column value with comma
		csv_data.push(csvrow.join(","));
	}

	// Combine each row data with new line character
	csv_data = csv_data.join('\n');

	// Call this function to download csv file 
	downloadCSVFile(csv_data);
}

function downloadCSVFile(csv_data) {

	// Create CSV file object and feed
	// our csv_data into it
	CSVFile = new Blob([csv_data], {
		type: "text/csv"
	});

	// Create to temporary link to initiate
	// download process
	var temp_link = document.createElement('a');

	// Download csv file
	temp_link.download = "reponses.csv";
	var url = window.URL.createObjectURL(CSVFile);
	temp_link.href = url;

	// This link should not be displayed
	temp_link.style.display = "none";
	document.body.appendChild(temp_link);

	// Automatically click the link to
	// trigger download
	temp_link.click();
	document.body.removeChild(temp_link);
}
</script>

<?php require('template.php'); ?>