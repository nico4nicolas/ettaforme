<?php session_start(); ?>

<?php require_once "common/classUsers.php"; ?>

<?php ob_start(); ?>

<?php

$display_page = false;

if (!($_SESSION['is_logged'] == true)) {
	echo "<div class=\"alert warning\">Vous n'êtes pas autorisé à voir cette page !</div>";
}
else {
    $users_obj = new Users();
    $user_obj = $users_obj->get_user_object($_SESSION['username']);

    if ($user_obj != false) {
        $display_page = true;
    }
}

if ( $display_page == true ) {
    echo '<h2>Modifier mes données</h2>';
    echo '<form action="user_edit.php" method="post">';
    echo '<article class="card">';
    echo '<header>';
    echo '  <input type="hidden" id="action" name="action" value="update" />';
    echo '  <p>Id</p>';
    echo '  <input id="uid" name="uid" value="'.$user_obj->get_id().'" readonly required />';
    echo '  <p>Prénom</p>';
    echo '  <input id="ufname" name="ufname" value="'.$user_obj->get_firstname().'" placeholder="Prénom" required />';
    echo '  <p>Nom</p>';
    echo '  <input id="ulname" name="ulname" value="'.$user_obj->get_lastname().'" placeholder="Nom" required />';
    echo '  <p>Mon équipe</p>';
    echo '  <select disabled>';
    echo '    <option selected disabled>Non implémenté</option>';
    echo '  </select>';
    echo '</header>';
    echo '<footer>';
    echo '  <button>Modifier</button>';
    echo '</footer>';
    echo '</article>';
    echo '</form>';

    echo '<form action="user_edit.php" method="post">';
    echo '<article class="card">';
    echo '<header>';
    echo '  <input type="hidden" id="action" name="action" value="update" />';
    echo '  <input type="hidden" id="uid" name="uid" value="'.$user_obj->get_id().'" />';
    echo '  <p>Mot de passe actuel</p>';
    echo '  <input id="upwd" name="upwd" type="password" placeholder="Mot de passe actuel" required />';
    echo '  <p>Nouveau mot de passe</p>';
    echo '  <input type="password" placeholder="Nouveau mot de passe" id="upwd_new" name="upwd_new" type="password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? \'Doit contenir au moins 6 caractères\' : \'\'); if(this.checkValidity()) form.upwd_confirm.pattern = this.value;" required/>';
    echo '  <p>Confirmer votre nouveau mot de passe</p>';
    echo '  <input type="password" placeholder="Confirmer votre nouveau mot de passe" id="upwd_confirm" name="upwd_confirm" type="password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? \'Les mots de passe sont différents\' : \'\');" required />';
    echo '</header>';
    echo '<footer>';
    echo '  <button>Modifier</button>';
    echo '</footer>';
    echo '</article>';
    echo '</form>';
}


?>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>