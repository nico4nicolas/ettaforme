<?php session_start(); ?>

<?php require_once "common/classUsers.php"; ?>

<?php ob_start(); ?>

<?php

$display_page = false;
$error_message = '<div class="alert error">Connexion refusée !</div>';

if(isset ( $_POST['action'] ) && !empty( $_POST['action'] ) ) {
	if( $_POST['action'] == 'login') {
		if(isset( $_POST['uid'] ) && !empty( $_POST['uid'] ) ) {
			$user_id = $_POST['uid'];
			if ( isset( $_POST['upwd'] ) && !empty( $_POST['upwd'] ) ) {
				$user_password = $_POST['upwd'];
				$users_obj = new Users();
				$user_obj = $users_obj->get_user_object($user_id);

				if ($user_obj != false) {
					if ($user_obj->is_password_correct($user_password)) {
						$display_page = true;
						$_SESSION['is_logged'] = true;
						$_SESSION['username'] = $user_id;
						$user_obj->set_new_login();
						$users_obj->update();
					}
				}
			}	
		}
	}
}
elseif('logout' == $_GET['action']) {
	$_SESSION['is_logged'] = false;
	unset($_SESSION['username']);
	$error_message = '<div class="alert info">Vous êtes maintenant déconnecté !</div>';
}
?>
<div>
<?php
if ( $display_page == true ) {
	echo '<div class="alert success">Connexion réussie ! <span class="closebtn" onclick="this.parentElement.style.display=\'none\'"">&times;</span> </div>';
	echo '<p>Utilisez le menu en haut à droite pour naviguer dans l\'application.</p>';
}
else {
    echo $error_message;
}
?>
</div>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>