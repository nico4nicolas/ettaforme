<?php session_start(); ?>

<?php include_once "config/config.php"; ?>
<?php require_once "common/classUsers.php"; ?>

<?php ob_start(); ?>

<?php
$is_user = false;
$is_admin = false;

if ($_SESSION['is_logged'] == true) {
    $is_user = true;
	$users_obj = new Users();
	$user_obj = $users_obj->get_user_object($_SESSION['username']);
	if ($user_obj != false) {
		if ($user_obj->get_is_admin() == true) {
			$is_admin = true;
		}
	}
}
?>

    <h2>Aide à l'utilisation</h2>

    <div id="content" class="flex one">
        <div class="content">
            <h3>Objectif</h3>
            <p>Cette application a pour but de collecter les données sur l'état de formes des jeunes joueurs et joueuses. Il leur est demandé de répondre aux questions une fois par semaine.</p>
        </div>
    </div>

    <div id="content" class="flex one">
        <div class="card">
            <header>Présentation de l'application</header>
            <section>
                <h4>Interface joueur</h4>
                <p>Le joueur doit sélectionner son équipe, son prénom puis son code. Si son code est valide, il aura accès aux questions.</p>
            </section>
            <section><a class="button" href="page_team.php">Interface joueur</a></section>
            <section>
                <h4>Espace entraineur</h4>
                <p>L'entraineur gère son effectif, il peut ajouter des joueurs, réinitialiser le code d'un joueur, désactiver un joueurs et voir les réponses des joueurs.</p>
            </section>

            <section>
<?php
if ($is_user == true) {
                echo '<a class="button" href="display_teams.php">🔒 Espace entraineur</a>';
}
else {
                echo '<label for="modal_login" class="button">🔒 Espace entraineur</label>';
}
?>
            </section>
<?php
if ($is_user == true) {
?>
            <section>
                <h4>Espace administrateur</h4>
                <p>L'administrateur gère les utilisateurs.</p>
            </section>
            <section><a class="button" href="display_users.php">🔒 Espace administrateur</a></section>
<?php
}
?>
        </div>
    </div>

<?php
if ($is_user == true) {
?>
    <div id="content" class="flex one">
        <div class="card">
            <header>Mes actions d'entraineur</header>
            <section>
                <h4>Voir la liste des équipes</h4>
                <p>C'est la page principale pour un entraineur. Toutes les équipes sont listées avec un lien vers l'effectif de l'équipe et un lien vers les réponses des joueurs.</p>
            </section>
            <section>
                <a class="button" href="display_teams.php">Les équipes</a>
            </section>
            <section>
                <h4>Gérer l'effectif</h4>
                <p>Au début de saison, l'effectif n'est pas renseigné, il faudra donc ajouter des joueurs. Lors de la création de joueurs, le code peut être laissé vide. Le joueur ne sera pas en mesure de répondre aux questions tant que le code n'a pas été renseigné.</p>
                <p>Le champ "joueur" est ce que le joueur va voir dans son interface. Il est recommandé de mettre le prénom + la première lettre du nom. Les espaces sont autorisées.</p>
                <p>Pour les joueurs existants, il est possible de modifier le code. Si aucun code n'avait été renseigné, cela activera le compte du joueur. Il n'y a pas de limitation imposée pour le code.</p>
                <p>Finalement, il est aussi possible de désactiver un joueur. Dans les faits, cela supprime le code du joueur ce qui l'empêche de répondre aux questions.</p>
            </section>
            <section>
                <a class="button" href="display_players.php">Les effectifs</a>
            </section>
            <section>
                <h4>Voir les réponses</h4>
                <p>Pour visualiser les réponses, il est recommandé d'utiliser un ordinateur.</p>
                <p>Les réponses peuvent être visualisées avec une échelle de couleur pour les réponses allant de 1 à 5.</p>
                <p>Il est possible de filtrer les réponses en cliquant sur l'identifiant d'un joueur. Pour revenir à la visualisation de tous les joueurs de l'équipe, il suffit de cliquer sur le bouton "Voir".</p>
                <p>Le téléchargement des réponses est possible en cliquant sur la disquette. Le format des réponses est "csv" ce qui permet de l'importer facilement dans un logiciel comme Microsoft Excel (il est toutefois recommandé d'utiliser Libre Office).</p>
            </section>
            <section>
                <a class="button" href="display_answers.php">Les réponses</a>
            </section>
        </div>
    </div>

    <div id="content" class="flex one">
        <div class="card">
            <header>Questions diverses</header>
            <section>
                <h4>Puis-je créer une équipe ?</h4>
                <p>Non, il s'agit d'une limitation voulue. Si votre équipe n'existe pas, merci de contacter un administrateur.</p>
            </section>
            <section>
                <h4>Est-il possible de changer mon identifiant ?</h4>
                <p>Ce n'est pas possible via l'interface. Si vous avez oublié votre mot de passe, merci de contacter un administrateur.</p>
            </section>
            <section>
                <h4>J'ai oublié mon mot de passe, comment faire ?</h4>
                <p>Si vous avez oublié votre mot de passe, un administrateur pourra le réinitialiser.</p>
            </section>
            <section>
                <h4>Est-il possible de supprimer un joueur de l'effectif ?</h4>
                <p>Il n'a pas été prévu d'ajouter cette fonctionnalité. Si vous souhaitez vraiment supprimer un joueur ou modifier son identifiant, c'est possible de façon exceptionnelle en demandant très gentiment à un administrateur.</p>
            </section>
        </div>
    </div>
<?php
}
?>

<div id="content" class="flex one">
        <div class="card">
            <header>A propos</header>
            <section>
                <h4>Version</h4>
                <p><?= $g_version ?></p>
            </section>
            <section>
                <h4>Réalisation</h4>
                <p>Par le SOM Basket, pour le SOM Basket.</p>
            </section>
        </div>
    </div>	

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>
