<?php session_start(); ?>

<?php require_once "common/classTeams.php"; ?>

<?php ob_start(); ?>

<?php
	$teams = new Teams();
	if ($teams->nb_teams == 0) {
		echo 'Aucune équipe créée';
	}
	else {
		echo '<p>Je joue avec les...</p>';
		echo '<form action="page_player.php" method="GET">';
		echo '<select name="tid" id="tid" onchange="this.form.submit()">';

		foreach ($teams->array_teams as $t) {
			echo '<option value="'.$t->get_id().'">'.$t->get_name().'</option>';
		}
		echo '<option value="" selected hidden></option>';
	}
?>
	</select>
	</br></br>
	<a class="button" id="prevBtn" href="index.php">Retour</a>
</form>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>