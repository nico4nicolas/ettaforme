<?php session_start(); ?>

<?php require_once "common/classUsers.php"; ?>

<?php ob_start(); ?>

<?php
$b_is_allowed = false;
$b_display_page = false;
$error_message = '<div class="alert warning">Vous n\'êtes pas autorisé à voir cette page !</div>';

if (($_SESSION['is_logged'] == true)) {

	$users_obj = new Users();
	$user_obj = $users_obj->get_user_object($_SESSION['username']);
	if ($user_obj != false) {
		if ($user_obj->get_is_admin() == true) {
			$b_is_allowed = true;
		}
	}
}

if ($b_is_allowed == true) {
	if(isset ( $_POST['action'] ) && !empty( $_POST['action'] ) ) {
		if( $_POST['action'] == 'insert') {
			if ($users_obj->insert($_POST['uid'], $_POST['ufname'], $_POST['ulname'])) {
				$new_user_obj = $users_obj->get_user_object($_POST['uid']);
				if ( isset( $_POST['upwd'] ) && !empty( $_POST['upwd'] ) ) {
					$new_user_obj->set_password($_POST['upwd']);
					$users_obj->update();
				}
			}
			$b_display_page = true;
		}
	}
	$error_message = '<div class="alert warning">Erreur : données manquantes !</div>';
}

if ($b_display_page == true) {
	if (false == $new_user_obj || null == $new_user_obj) {
		echo '<div class="alert error">Erreur : utilisateur non ajouté !</div>';
	}
	else {
		echo '<div class="alert success">'.$new_user_obj->get_firstname().' ajouté !</div>';
	}
}
else {
	echo $error_message;
}
echo '<a class="button" href="display_users.php">Retour</a>';

?>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>