<?php session_start(); ?>

<?php require_once "common/classPlayers.php"; ?>

<?php ob_start(); ?>

<?php

if (!($_SESSION['is_logged'] == true)) {
	echo "<div class=\"alert warning\">Vous n'êtes pas autorisé à voir cette page !</div>";
}
else {
	$b_is_player_added = false;
	$error_message = '';

	if( isset ( $_POST['action'] ) && !empty( $_POST['action'] ) ) {
		if( $_POST['action'] == 'insert') {
			if( isset( $_POST['tid'] ) && !empty( $_POST['tid'] ) ) {
				$team_id = $_POST['tid'];
				$players_obj = new Players($team_id);

				$id = $players_obj->insert($_POST['pfname'], $_POST['plname']);

				if ( false != $id ) {
					$player_obj = $players_obj->get_player_object($id);

					if ( isset( $_POST['pcode'] ) && !empty( $_POST['pcode'] ) ) {
						$player_obj->set_code($_POST['pcode']);
						$players_obj->update();
					}
					$b_is_player_added = true;
				}
			}
			else {
				$error_message = '<div class="alert warning">Erreur : données manquantes !</div>';
			}
		}
	}
	else {
		if(isset( $_GET['tid'] ) && !empty( $_GET['tid'] ) ) {
			$team_id = $_GET['tid'];
		}
		else {
			$error_message = '<div class="alert warning">Erreur : données manquantes !</div>';
		}
	}

	if ( $b_is_player_added == true ) {
			echo '<div class="alert success">'.$player_obj->get_fullname().' ajouté !</div>';
	}
	else {
		echo '<div class="alert error">Erreur : joueur non ajouté !</div>';
		echo $error_message;
	}
	echo '<a class="button" href="display_players.php?tid='.$team_id.'">Retour</a>';
}
?>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>