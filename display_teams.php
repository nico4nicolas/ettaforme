<?php session_start(); ?>

<?php require_once "common/classTeams.php"; ?>

<?php ob_start(); ?>


<?php

if (!($_SESSION['is_logged'] == true)) {
	echo "<div class=\"alert warning\">Vous n'êtes pas autorisé à voir cette page !</div>";
}
else {
	echo '<table>';
	$teams = new Teams();
	echo "<tr><th>Equipe</th><th>Actions</th></tr>";
	foreach ($teams->array_teams as $t) {
		echo '<tr><td>'.$t->get_name().'</td>';
		echo '<td><a class="button" href="display_players.php?tid='.$t->get_id().'">👥 Effectif</a> ';
		echo '<a class="button" href="display_answers.php?tid='.$t->get_id().'">🗒 Réponses</a></td>';
		echo '</tr>';
	}
	echo '</table>';
}
?>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>