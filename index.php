<?php session_start(); ?>

<?php ob_start(); ?>

<div class="flex one">
    <h1><a style="text-align: center;" class="button mainButton stack" href="page_team.php">🏀<br>Je joue</a></h1>
    <h1><label style="text-align: center;" for="modal_login" class="button mainButton stack">🔒<br>J'entraine</label></h1>
</div>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>