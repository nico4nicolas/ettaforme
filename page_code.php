<?php session_start(); ?>

<?php require_once "common/classPlayers.php"; ?>

<?php ob_start(); ?>

<?php

$display_page = false;

if(isset( $_GET['tid'] ) && !empty( $_GET['tid'] ) ) {
	$team_id = $_GET['tid'];
	if(isset( $_GET['pid'] ) && !empty( $_GET['pid'] ) ) {
		$players_obj = new Players($team_id);
		$player_obj = $players_obj->get_player_object($_GET['pid']);
		if ( false != $player_obj ) {
			$display_page = true;
		}
		else {
			echo "<div class=\"alert error\">Le joueur n'a pas été trouvé !</div>";
		}
	}
	else {
		echo "<div class=\"alert warning\">Pas de joueur sélectionné !</div>";
	}
}
else {
	echo "<div class=\"alert warning\">Pas d'équipe sélectionnée !</div>";
}

if ( $display_page === true ) {
	echo '<p>Bonjour '.$player_obj->get_firstname().' ! Quel est ton code ?</p>';
	echo '<form action="page_questions.php" method="POST">';
	echo '<input type="hidden" id="team" name="team" value="'.$team_id.'" />';
	echo '<input type="hidden" id="player" name="player" value="'.$player_obj->get_id().'" />';
	echo '<input type="password" id="player_code" name="code">';
	echo '</br></br>';
	echo '<a class="button" id="prevBtn" href="page_player.php?tid='.$team_id.'">Retour</a>';
	echo '<button id="nextBtn" type="submit">Vérifier</button>';
	echo '</form>';
}
else {
	echo 'Erreur : la page ne peut pas s\'afficher !</br>';
	echo '<a href=\"page_team.php\">Sélection équipe</a>';
}

?>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>