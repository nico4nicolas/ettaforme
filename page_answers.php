<?php session_start(); ?>

<?php include_once "config/config.php"; ?>
<?php require_once "common/classPlayers.php"; ?>

<?php ob_start(); ?>
<?php

if(!isset( $_POST["code"] ) or $_POST["code"] == "") {
	header("Location: page_team.php");
	die();
}

if(isset( $_POST['team'] ) && !empty( $_POST['team'] ) ) {
	$team = $_POST['team'];
	if(isset( $_POST['player'] ) && !empty( $_POST['player'] ) ) {
		$players_obj = new Players($team);
		$player_obj = $players_obj->get_player_object($_POST['player']);
	}
	else {
		echo "<div class=\"alert warning\">Pas de joueur sélectionné !</div>";
	}
}
else {
	echo "<div class=\"alert warning\">Pas d'équipe sélectionnée !</div>";
}

$values = array();
for ($q = 1; $q <= $g_nb_questions; $q++) {
	$question_id = "q" . $q;
	if(isset( $_POST[$question_id] ) && !empty( $_POST[$question_id] ) ) {
		array_push($values, $_POST[$question_id]);
	}
}

if ( $player_obj->is_code_correct($_POST["code"]) ) {
?>
	<div>
		<div>
<?php
		$date = new \DateTime();
		$line_answers = $date->format('Y-m-d');
		$line_answers .= ",".$date->format('H:i:s');
		$line_answers .= ",".$player_obj->get_id();
		for ($q = 1; $q <= $g_nb_questions; $q++) {
			$question_id = "q" . $q;
			//echo $question_id." : ".$values[$q-1]."</br>";
			$line_answers .= ",".$values[$q-1];
		}
		$line_answers .= "\n";
		$file_answers = fopen("data/teams/".$team."/answers.csv","a");
		fwrite($file_answers, $line_answers); 
		fclose($file_answers);
?>
		</div>
		<div>
			<h3>Merci d'avoir complété le questionnaire !</h3> <span>Les réponses ont été sauvegardées, à la semaine prochaine !</span>
		</div>
		<br><a class="button" href="page_team.php">Retour</a>
	</div>

<?php
}
else {
	header("Location: page_team.php");
	die();
}

?>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>