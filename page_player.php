<?php session_start(); ?>

<?php require_once "common/classPlayers.php"; ?>

<?php ob_start(); ?>

<?php

$display_page = false;

if(isset( $_GET['tid'] ) && !empty( $_GET['tid'] ) ) {
	$team_id = $_GET['tid'];
	$display_page = true;
}
else {
	echo "<div class=\"alert warning\">Pas d'équipe sélectionnée !</div>";
}

if ( $display_page === true ) {
	$players = new Players($team_id);
	if ( $players->nb_players > 0 ) {
		echo "<p>Je suis...</p>";
		echo "<form action=\"page_code.php\" method=\"GET\">";
		echo "<input type=\"hidden\" id=\"tid\" name=\"tid\" value=\"".$team_id."\" />";
		echo '<select name="pid" id="pid" onchange="this.form.submit()">';
		
		foreach ($players->array_players as $p) {
			echo "<option value=\"".$p->get_id()."\">".$p->get_fullname()."</option>";
		}
		echo '<option value="" selected hidden></option>';
		
		echo '</select>';
		echo '</br></br>';
		echo '<a class="button" id="prevBtn" href="page_team.php">Retour</a>';
		echo '</form>';
	}
	else {
		echo '<div class="alert info">Effectif non renseigné !</div>';
		echo '<a class="button" id="prevBtn" href="page_team.php">Retour</a>';
	}
}
else {
	echo '<div class="alert warning">Erreur : la page ne peut pas s\'afficher !</div>';
	echo '<a class="button" href="page_team.php">Retour</a>';
}
?>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>