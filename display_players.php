<?php session_start(); ?>

<?php require_once "common/classTeams.php"; ?>
<?php require_once "common/classPlayers.php"; ?>

<?php ob_start(); ?>

<?php

if (!($_SESSION['is_logged'] == true)) {
	echo "<div class=\"alert warning\">Vous n'êtes pas autorisé à voir cette page !</div>";
}
else {
	$display_page = true;

	if(isset( $_GET['tid'] ) && !empty( $_GET['tid'] ) ) {
		$team_id= $_GET['tid'];
	}
	else {
		echo "<div class=\"alert warning\">Pas d'équipe sélectionnée !</div>";
		$display_page = false;
	}
?>
		<form action="display_players.php" method="GET">
			<label for="tid">Je veux voir l'effectif des</label>
			<select name="tid" id="tid" onchange="this.form.submit()">
<?php
			$teams = new Teams();
			foreach ($teams->array_teams as $t) {
				if($t->get_id() == $team_id) {
					echo "<option value=\"".$t->get_id()."\" selected>".$t->get_name()."</option>";
				}
				else {
					echo "<option value=\"".$t->get_id()."\">".$t->get_name()."</option>";
				}
			}
			if ( $display_page == false ) {
			    echo '<option value="" selected hidden></option>';
			}
?>
			</select>
		</form>
<?php
	if ( $display_page === true ) {
		$players = new Players($team_id);

		echo '<table>';
		
		echo '<tr><td colspan="100%">';
		
		$modal_id = 'modal_addplayer';
		
		echo '<label for="'.$modal_id.'" class="button success">➕ Ajouter un joueur</label>';
		
		echo '<div class="modal">';
		echo '  <input id="'.$modal_id.'" type="checkbox" />';
		echo '  <label for="'.$modal_id.'" class="overlay"></label>';
		echo '  <article>';
		echo '	<header>';
		echo '	  <h3>Ajouter un joueur</h3>';
		echo '	  <label for="'.$modal_id.'" class="close">&times;</label>';
		echo '	</header>';
		echo '	<section class="content">';
		echo '		<form action="player_add.php" method="post" name="add_player">';
		echo '			<input type="hidden" id="action" name="action" value="insert" />';
		echo '			<input type="hidden" id="tid" name="tid" value="'.$team_id.'" />';
		echo '			<label for="pfname">Prénom</label></br>';
		echo '			<input type="text" id="pfname" name="pfname" required ></br>';
		echo '			<label for="plname">Nom</label></br>';
		echo '			<input type="text" id="plname" name="plname"></br>';
		echo '			<label for="pcode">Code</label></br>';
		echo '			<input type="password" id="pcode" name="pcode"></br>';
		echo '			<button type="submit">Ajouter</button>';
		echo '			<label for="'.$modal_id.'" class="button dangerous">Annuler</label>';
		echo '		</form> ';
		echo '	</section>';
		echo '  </article>';
		echo '</div>';
		
		echo '</td></tr>';

		echo '<tr><th>Joueur</th><th>Actions</th></tr>';
		
		foreach ($players->array_players as $p) {

			echo '<tr>';
			echo '<td>'.$p->get_fullname().'</td>';
			echo '<td>';
			
			$modal_id = 'modal_edit_'.$p->get_id();
			
			echo '<label for="'.$modal_id.'" class="button warning">Modifier</label> ';

			if ($p->is_code_correct("")) {;
				echo '<a class="button" disabled>Désactiver</a>';
			}
			else {
				echo '<a class="button error" onclick="return confirm(\'Désactiver '.$p->get_fullname().' ?\');" href="player_disable.php?action=disable&tid='.$team_id.'&pid='.$p->get_id().'">Désactiver</a>';
				
			}
			
			echo '<div class="modal">';
			echo '  <input id="'.$modal_id.'" type="checkbox" />';
			echo '  <label for="'.$modal_id.'" class="overlay"></label>';
			echo '  <article>';
			echo '	<header>';
			echo '	  <h3>Modifier un joueur</h3>';
			echo '	  <label for="'.$modal_id.'" class="close">&times;</label>';
			echo '	</header>';
			echo '	<section class="content">';
			echo '		<form action="player_edit.php" method="post" name="edit_player">';
			echo '			<input type="hidden" id="action" name="action" value="update" />';
			echo '			<input type="hidden" id="tid" name="tid" value="'.$team_id.'" />';
			echo '			<input type="hidden" id="pid" name="pid" required readonly value="'.$p->get_id().'" /></br>';
			echo '			<label for="pfname">Prénom</label></br>';
			echo '			<input type="text" id="pfname" name="pfname" value="'.$p->get_firstname().'" required></br>';
			echo '			<label for="plname">Nom</label></br>';
			echo '			<input type="text" id="plname" name="plname" value="'.$p->get_lastname().'"></br>';
			echo '			<label for="pcode">Code</label></br>';
			echo '			<input type="password" id="pcode" name="pcode"></br>';
			echo '			<button type="submit">Modifier</button>';
			echo '	  		<label for="'.$modal_id.'" class="button dangerous">Annuler</label>';
			echo '		</form> ';
			echo '	</section>';
			echo '  </article>';
			echo '</div>';
			
			echo '</td></tr>';
		}
		echo '</table>';
	}
}
?>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>